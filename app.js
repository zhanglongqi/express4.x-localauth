/**
*Module dependencies
*/
let
  express = require('express'),
  logger = require('morgan'),
  bParser = require('body-parser'),
	session = require('express-session');
//==============================================================================
/**
*Create app instance
*/
var app = express();
//==============================================================================
/**
*Module Variables
*/
var
  config = require('./config/config'),
  port = process.env.PORT || 3030,
  env = config.env,
	router = require('./routes/routes');

  app.locals.errMsg = app.locals.errMsg || null;
//==============================================================================
/**
*Module Settings and Config
*/
app.set('port', port);
app.set('env', env);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

//==============================================================================
/**
*Middleware
*/
app.use(logger('dev'));

app.use(bParser.json());

app.use(bParser.urlencoded({ extended: true }));

app.use(session({
	name: 'express_local.sess', secret: 'qwertyuiop123456789', resave: false,
  saveUninitialized: false, cookie: {maxAge: 1000 * 60 * 15}}));
//==============================================================================
/**
*Routes
*/
app.use('/', router);
//==============================================================================
/**
*Export Module
*/
module.exports = app;
//==============================================================================
