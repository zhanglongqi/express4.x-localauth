/**
 *Module dependencies
 */
var
	bcrypt = require('bcrypt-nodejs');
//==============================================================================
/**
 *Module Variables
 */

let users = [{
	username: 'aaa',
	email: 'longqi90@gmail.com',
	password: '$2a$08$zAFTZjQCpUtu/O6z9tx2qOBHQ4ObJc2yLHEE1X75wdvhK3uo.ioM2', // password is 'bbb'
	created_on: ''
}];


class UserModel2 {
	constructor() {

	}

	static findById(id, cb) {
		users.forEach((user) => {
			if (user.username === id)
				cb(null, user);
		});
	}

	static findOne(new_user, cb) {
		let user1 = null;
		users.forEach((user) => {
			if (user.email === new_user.email) {
				user1 = user;
			}
		});

		if (user1) {
			let user = new UserModel2();
			user.username = user1.username;
			user.email = user1.email;
			user.password = user1.password;
			cb(null, user);
		}
		else
			cb(null, user1);

	}

	static generateHash(password) {
		return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
	};

	validPassword(password) {
		return bcrypt.compareSync(password, this.password);
	};

	async save() {
		users.push({
			username: this.username,
			email: this.email,
			password: this.password,
			created_on: Date.now()
		});
		let saving_result = true;
		return new Promise((resolve, reject) => {
			setTimeout(() => {
				if (saving_result)
					resolve();
				else
					reject();
			}, 100);
		});
	}
}

//==============================================================================
/**
 *Export Module
 */
module.exports =
	{
		User: UserModel2,
		USERS: users,
	};
//==============================================================================