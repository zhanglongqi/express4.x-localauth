/**
 *Module dependencies
 */
var
	passport = require('passport'),
	User = require('../models/users').User,
	utilities = require('../models/utilities');
/**
 *Module variables
 */
var
	errHandler = utilities.errHandler,
	LocalStrategy = require('passport-local').Strategy;
/**
 *Configuration and Settings
 */
passport.serializeUser(function (user, done) {
	done(null, user.username);
});

passport.deserializeUser(function (username, done) {
	User.findById(username, function (err, user) {
		if (err) {
			console.error('There was an error accessing the records of user with username: ' + username);
			return console.log(err.message);
		}
		return done(null, user);
	})
});
/**
 *Strategies
 */
//---------------------------Local Strategy-------------------------------------
passport.use('local-signup', new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true
	},
	function (req, email, password, done) {
		User.findOne({email: email}, function (err, user) {
			if (err) {
				return errHandler(err);
			}
			if (user) {
				console.log('user already exists');
				return done(null, false, {errMsg: 'email already exists'});
			}
			else {
				var newUser = new User();
				newUser.username = req.body.username;
				newUser.email = email;
				newUser.password = User.generateHash(password);
				newUser.save().then(() => {
					console.log('New user successfully created...', newUser.username);
					console.log('email', email);
					console.log(newUser);
					return done(null, newUser);
				}).catch(reason => {
					return done(null, false, {message: 'database error:' + reason});
				});
			}
		});
	}));
//---------------------------local login----------------------------------------
passport.use('local-login', new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true
	},
	function (req, email, password, done) {
		User.findOne({email: email}, function (err, user) {
			if (err) {
				return errHandler(err);
			}
			if (!user) {
				return done(null, false, {
					errMsg: 'User does not exist, please' +
						' <a class="errMsg" href="/signup">signup</a>'
				});
			}
			if (!user.validPassword(password)) {
				return done(null, false, {errMsg: 'Invalid password try again'});
			}
			return done(null, user);
		});

	}));
/**
 *Export Module
 */
module.exports = passport;
